package com.basketapp.basket_app.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.basketapp.basket_app.dto.UtilisateurDto;
import com.basketapp.basket_app.models.Role;
import com.basketapp.basket_app.models.Utilisateur;
import com.basketapp.basket_app.services.UtilisateurService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UtilisateurRESTController {
	
	@Autowired
	UtilisateurService utilisateurService;

	@Autowired
	private ModelMapper modelMapper;
	
	//Get all users
	@RequestMapping(path = "all",method = RequestMethod.GET)
	public List<UtilisateurDto> getAllUtilisateurs() {
		return utilisateurService.getAllUtilisateurs().stream().map(utilisateur -> modelMapper.map(utilisateur, UtilisateurDto.class))
			.collect(Collectors.toList());
	}
		
    //get user by id
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
	public ResponseEntity<UtilisateurDto> getUtilisateurById(@PathVariable("id") Long id) {
	    Utilisateur utilisateur = utilisateurService.getUtilisateur(id);

		// convert entity to DTO
		UtilisateurDto utilisateurResponse = modelMapper.map(utilisateur, UtilisateurDto.class);
		return ResponseEntity.ok().body(utilisateurResponse);
	}

	//get user by username
	@RequestMapping(value = "/nom/{username}", method = RequestMethod.GET)
	public Utilisateur getUtilisateurByUsername(@PathVariable("username") String nom) {
		return utilisateurService.findUserByUsername(nom);
	}

	//get user by role
	@RequestMapping(value = "/role/{idRole}",method = RequestMethod.GET)
	public List<Utilisateur> rechercheUtilisateurByRole(@PathVariable("idRole") Long id) {
		return utilisateurService.rechercheUtilisateurByRole(id);
	}

	//create user
	@RequestMapping(method = RequestMethod.POST)
	public Utilisateur createUtilisateur(@RequestBody Utilisateur utilisateur,Role role,UtilisateurDto utilisateurDto ) {
		Utilisateur u=utilisateurService.saveUtilisateur(utilisateur);
		utilisateurService.saveUtilisateurDto(utilisateur, utilisateurDto);
		return utilisateurService.addRoleToUtilisateur(u.getUsername(),role.getRole());
	}
		
	//update user
	@RequestMapping(method = RequestMethod.PUT)
	public Utilisateur updateUtilisateur(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.updateUtilisateur(utilisateur);
	}
		
	//get user by id
	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
	public void deleteUtilisateur(@PathVariable("id") Long id){
		utilisateurService.deleteUtilisateurById(id);
	}
}
