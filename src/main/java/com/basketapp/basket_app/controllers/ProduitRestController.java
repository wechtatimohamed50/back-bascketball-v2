package com.basketapp.basket_app.controllers;

import java.util.List;

import com.basketapp.basket_app.dao.ProduitRepos;
import com.basketapp.basket_app.models.Produit;
import com.basketapp.basket_app.services.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/produit")
@CrossOrigin
public class ProduitRestController {
	
	@Autowired
	ProduitService produitService;

	@Autowired
	ProduitRepos produitRepos;
	

	//Get produit by Id
	@RequestMapping(value="/{id}",method = RequestMethod.GET)
	public Produit getProduit(@PathVariable("id") Long id) {
		return produitService.getProduitById(id);
	}
			
	//Create produit
	@RequestMapping(method = RequestMethod.POST)
	public Produit createProduit(@RequestBody Produit produit) {
		return produitService.createProduit(produit);
	}
			
	//Update produit
	@RequestMapping(method = RequestMethod.PUT)
	public Produit modifierProduit(@RequestBody Produit produit) {
		return produitService.updateProduit(produit);
	}
			
	//Delete produit
	@RequestMapping(value="/{id}",method = RequestMethod.DELETE)
	public void deleteProduit(@PathVariable("id") Long id){
		produitService.deleteProduit(id);
	}

	//recherche produit par nom
	@RequestMapping(value="/{nom}",method = RequestMethod.GET)
	public List<Produit> rechercherProduitByNom(@PathVariable("nom") String nom){
	    return produitService.rechercherProduitByNom(nom);
	} 

	//recherche produit par 
	@RequestMapping(value="/{part}",method = RequestMethod.GET)
	public List<Produit> findProduitByPart(@PathVariable("part") String part){
		return produitService.rechercherProduitByNom(part);
	}

	//get all produits
	@RequestMapping(method = RequestMethod.GET)
	public List<Produit> rechercherProduitByPart(){
		return produitRepos.findAll();
	}

	//favoriser un produit
	@RequestMapping(value="/{id}",method = RequestMethod.POST)
	public Produit favoriserProduit(@PathVariable("id") Long id) {
			return produitService.favoriserProduit(id);
	}
}
