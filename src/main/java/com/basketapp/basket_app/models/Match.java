package com.basketapp.basket_app.models;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.basketapp.basket_app.dto.UtilisateurDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Match /* extends Evenement*/{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long idMatch;
    private String type;
    
    @ManyToOne
    private Ligue ligue;
 
    @ManyToOne
    private Equipe equipeAdversaire;
    
    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List <UtilisateurDto> joueurs;

    @ManyToOne
    private UtilisateurDto coach;

    @ManyToOne
    private Stade stade;
}
