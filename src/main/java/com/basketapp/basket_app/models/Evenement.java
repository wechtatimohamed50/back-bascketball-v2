package com.basketapp.basket_app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Evenement {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEvenement;
	private String titre; 
	private String description;
	private	String date; 
	private String debut;
	private String fin ;
	
}
